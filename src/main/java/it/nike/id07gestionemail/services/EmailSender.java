package it.nike.id07gestionemail.services;

import org.springframework.mail.javamail.JavaMailSender;

import app.nike.models.Email;


public interface EmailSender {

	public JavaMailSender getJavaMailSender();
	
	public void inviaEmail(Email email);

}
